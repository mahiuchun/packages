# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=qqc2-desktop-style
pkgver=5.74.0
pkgrel=0
pkgdesc="QtQuickControls 2 style that uses QWidget's QStyle for painting"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0-only AND LGPL-3.0-only"
depends=""
makedepends="cmake extra-cmake-modules kconfig-dev kconfigwidgets-dev
	kiconthemes-dev kirigami2-dev qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev qt5-qtx11extras-dev"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/qqc2-desktop-style-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild .
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="88c91d5933bc2c0020a574be05a851aea54382b6c1dd5f459cba792e4ac784782bc88a36670c28f2df29188cf001bf2c9af98b3e9bc84dd59c110fd7bf936258  qqc2-desktop-style-5.74.0.tar.xz"
