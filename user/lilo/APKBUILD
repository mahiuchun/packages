# Maintainer: Max Rees <maxcrees@me.com>
pkgname=lilo
pkgver=24.2
pkgrel=1
pkgdesc="Minimal BIOS bootloader for x86 systems"
url="https://www.joonet.de/lilo/"
arch="pmmx x86 x86_64"
options="!check"  # No test suite.
license="BSD-3-Clause AND GPL-2.0+ AND Public-Domain"
depends="perl"
makedepends="dev86 linux-headers lvm2-dev sharutils"
subpackages="$pkgname-doc"
source="https://www.joonet.de/lilo/ftp/sources/lilo-$pkgver.tar.gz
	adelie.patch
	cflags.patch
	initramfs.patch
	musl.patch
	partuuid.patch
	lilo.conf.template
	lilo.easy-boot
	"

build() {
	export CFLAGS="$CFLAGS -fno-strict-aliasing"
	make
}

package() {
	make DESTDIR="$pkgdir" install

	rm -rf "$pkgdir"/etc/lilo/kernel \
		"$pkgdir"/etc/lilo/initramfs \
		"$pkgdir"/usr/sbin/mkrescue \
		"$pkgdir"/usr/share/man/man8/mkrescue.8

	mkdir "$pkgdir"/boot/lilo/gfx
	mv "$pkgdir"/boot/lilo/*.bmp \
		"$pkgdir"/boot/lilo/*.dat \
		"$pkgdir"/boot/lilo/gfx

	install -Dm644 "$srcdir"/lilo.conf.template \
		"$pkgdir"/etc/lilo/lilo.conf.template
	mv "$pkgdir"/etc/lilo/lilo.conf_example \
		"$pkgdir"/etc/lilo/lilo.conf.example

	install -Dm755 "$srcdir"/lilo.easy-boot \
		"$pkgdir"/etc/easy-boot.d/50-lilo
}

sha512sums="4437cae21345f483194a5dc95f686f3f3cb2beec78faae3fba959db25eae29fe2c56732e055c05f1d101682c5d442cdc9561fae8074f61f5537dde0413204c54  lilo-24.2.tar.gz
cf8dab563e88cc7b280f5740c3b0f25049b9ce27c77a67e7b0c55dc21a158f8b2998f0c7743f11bcc4ae893697e6b6cc56054a40919899bf2d6d79b4e1a59190  adelie.patch
ed1ffb03dbbbf6fe6974d045005f72b3657d470cd3f3556d582e09a38836090c3e75a684a1dcfbe7ade91e5ada3706b712ae7111460df85454518a746c60bde4  cflags.patch
98e26319a747e6cc7e9d7c695d20e511ef9d196dfc312403c7171380e75edbbdec6af39c0f1670956bcd7ec9ff203505c4001962c767f651b730da31830ed35f  initramfs.patch
1c1ce61d2d7c88c1ba66e1415b36edb6743f46a50857b5bcd2b6b28a0711e4de3f0a75c352a5997bc9b5d50b0754c49a3f69b736e07a6e389c02da004289c64f  musl.patch
3962bf30a866f31fa5263618210d61ac5663117cac1d5a52a5e14f3665ff8edfd628c80807591f3da5f2e6908b8c0af9b19fac32f6ab90e6c4755bf79cb34d5a  partuuid.patch
489f57d29ee6607fb7040a33655bc369a510290804ae11686e02ba91d6cd3401175fa93d8d74da6058f03126b924004d28d5145ec5df70be4d04acba112c5729  lilo.conf.template
cdf4ea20a678cc01149c06556fe6b3b9b70a91aad7e44ffb7728b677297de8f68d4a5037aa44414bd606c838621fc9191c5ad45f909538b852c1f770b4fcb2e4  lilo.easy-boot"
