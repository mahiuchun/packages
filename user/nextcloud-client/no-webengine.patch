From 61561d454fec726741a938b81149fa0e2d7db4d6 Mon Sep 17 00:00:00 2001
From: Max Rees <maxcrees@me.com>
Date: Mon, 24 Feb 2020 21:32:58 -0600
Subject: [PATCH] Add support for compiling without QtWebEngine (webflow /
 flow2 support)

Signed-off-by: Max Rees <maxcrees@me.com>
---
 CMakeLists.txt                       |  7 +++++++
 src/CMakeLists.txt                   |  6 +++++-
 src/gui/CMakeLists.txt               | 25 +++++++++++++++-------
 src/gui/accountmanager.cpp           |  2 ++
 src/gui/creds/credentialsfactory.cpp |  4 ++++
 src/gui/wizard/owncloudsetuppage.cpp |  4 ++++
 src/gui/wizard/owncloudwizard.cpp    | 31 +++++++++++++++++++++++++++-
 src/gui/wizard/owncloudwizard.h      |  6 ++++++
 src/libsync/networkjobs.cpp          |  8 +++++++
 9 files changed, 83 insertions(+), 10 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index f8e92e250..90719c222 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -167,6 +167,13 @@ if(NO_SHIBBOLETH)
    add_definitions(-DNO_SHIBBOLETH=1)
 endif()
 
+# Disable webengine-based components
+option(NO_WEBENGINE "Build without webflow / flow2 support so QtWebEngine isn't required" OFF)
+if(NO_WEBENGINE)
+   message("Compiling without webengine")
+   add_definitions(-DNO_WEBENGINE=1)
+endif()
+
 if(APPLE)
   set( SOCKETAPI_TEAM_IDENTIFIER_PREFIX "" CACHE STRING "SocketApi prefix (including a following dot) that must match the codesign key's TeamIdentifier/Organizational Unit" )
 endif()
diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
index 9f854b006..881daec8e 100644
--- a/src/CMakeLists.txt
+++ b/src/CMakeLists.txt
@@ -4,11 +4,15 @@ endif()
 
 set(synclib_NAME ${APPLICATION_EXECUTABLE}sync)
 
-find_package(Qt5 5.6 COMPONENTS Core Network Xml Concurrent WebEngineWidgets WebEngine REQUIRED)
+find_package(Qt5 5.6 COMPONENTS Core Network Xml Concurrent REQUIRED)
 if (Qt5Core_VERSION VERSION_LESS 5.9.0)
 message(STATUS "For HTTP/2 support, compile with Qt 5.9 or higher.")
 endif()
 
+if(NOT NO_WEBENGINE)
+    find_package(Qt5 5.6 COMPONENTS WebEngineWidgets WebEngine REQUIRED)
+endif()
+
 if(NOT TOKEN_AUTH_ONLY)
     find_package(Qt5Keychain REQUIRED)
 endif()
diff --git a/src/gui/CMakeLists.txt b/src/gui/CMakeLists.txt
index 4372a9f23..d62f23d1e 100644
--- a/src/gui/CMakeLists.txt
+++ b/src/gui/CMakeLists.txt
@@ -108,24 +108,17 @@ set(client_SRCS
     creds/credentialsfactory.cpp
     creds/httpcredentialsgui.cpp
     creds/oauth.cpp
-    creds/flow2auth.cpp
     creds/keychainchunk.cpp
-    creds/webflowcredentials.cpp
-    creds/webflowcredentialsdialog.cpp
     wizard/postfixlineedit.cpp
     wizard/abstractcredswizardpage.cpp
     wizard/owncloudadvancedsetuppage.cpp
     wizard/owncloudconnectionmethoddialog.cpp
     wizard/owncloudhttpcredspage.cpp
     wizard/owncloudoauthcredspage.cpp
-    wizard/flow2authcredspage.cpp
-    wizard/flow2authwidget.cpp
     wizard/owncloudsetuppage.cpp
     wizard/owncloudwizardcommon.cpp
     wizard/owncloudwizard.cpp
     wizard/owncloudwizardresultpage.cpp
-    wizard/webviewpage.cpp
-    wizard/webview.cpp
     wizard/slideshow.cpp
 )
 
@@ -138,6 +131,18 @@ IF(NOT NO_SHIBBOLETH)
     )
 endif()
 
+IF(NOT NO_WEBENGINE)
+    list(APPEND client_SRCS
+        creds/flow2auth.cpp
+        creds/webflowcredentials.cpp
+        creds/webflowcredentialsdialog.cpp
+        wizard/flow2authcredspage.cpp
+        wizard/flow2authwidget.cpp
+        wizard/webviewpage.cpp
+        wizard/webview.cpp
+    )
+endif()
+
 set(updater_SRCS
     updater/ocupdater.cpp
     updater/updateinfo.cpp
@@ -298,7 +303,11 @@ else()
 endif()
 
 add_library(updater STATIC ${updater_SRCS})
-target_link_libraries(updater ${synclib_NAME} Qt5::Widgets Qt5::Network Qt5::Xml Qt5::WebEngineWidgets)
+if(NOT NO_WEBENGINE)
+    target_link_libraries(updater ${synclib_NAME} Qt5::Widgets Qt5::Network Qt5::Xml Qt5::WebEngineWidgets)
+else()
+    target_link_libraries(updater ${synclib_NAME} Qt5::Widgets Qt5::Network Qt5::Xml)
+endif()
 target_include_directories(updater PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
 
 set_target_properties( ${APPLICATION_EXECUTABLE} PROPERTIES
diff --git a/src/gui/accountmanager.cpp b/src/gui/accountmanager.cpp
index eec375180..2d7b0bc30 100644
--- a/src/gui/accountmanager.cpp
+++ b/src/gui/accountmanager.cpp
@@ -253,6 +253,7 @@ AccountPtr AccountManager::loadAccountHelper(QSettings &settings)
         acc->setUrl(urlConfig.toUrl());
     }
 
+#ifndef NO_WEBENGINE
     // Migrate to webflow
     if (authType == QLatin1String("http")) {
         authType = "webflow";
@@ -266,6 +267,7 @@ AccountPtr AccountManager::loadAccountHelper(QSettings &settings)
             settings.remove(key);
         }
     }
+#endif
 
     qCInfo(lcAccountManager) << "Account for" << acc->url() << "using auth type" << authType;
 
diff --git a/src/gui/creds/credentialsfactory.cpp b/src/gui/creds/credentialsfactory.cpp
index 6062f70eb..723196d08 100644
--- a/src/gui/creds/credentialsfactory.cpp
+++ b/src/gui/creds/credentialsfactory.cpp
@@ -21,7 +21,9 @@
 #ifndef NO_SHIBBOLETH
 #include "creds/shibbolethcredentials.h"
 #endif
+#ifndef NO_WEBENGINE
 #include "creds/webflowcredentials.h"
+#endif
 
 namespace OCC {
 
@@ -40,8 +42,10 @@ namespace CredentialsFactory {
         } else if (type == "shibboleth") {
             return new ShibbolethCredentials;
 #endif
+#ifndef NO_WEBENGINE
         } else if (type == "webflow") {
             return new WebFlowCredentials;
+#endif
         } else {
             qCWarning(lcGuiCredentials, "Unknown credentials type: %s", qPrintable(type));
             return new DummyCredentials;
diff --git a/src/gui/wizard/owncloudsetuppage.cpp b/src/gui/wizard/owncloudsetuppage.cpp
index 11b1fc80c..30df7cc7f 100644
--- a/src/gui/wizard/owncloudsetuppage.cpp
+++ b/src/gui/wizard/owncloudsetuppage.cpp
@@ -144,7 +144,11 @@ void OwncloudSetupPage::slotLogin()
 void OwncloudSetupPage::slotGotoProviderList()
 {
     _ocWizard->setRegistration(true);
+#ifndef NO_WEBENGINE
     _ocWizard->setAuthType(DetermineAuthTypeJob::AuthType::WebViewFlow);
+#else
+    _ocWizard->setAuthType(DetermineAuthTypeJob::AuthType::Basic);
+#endif
     _authTypeKnown = true;
     _checking = false;
     emit completeChanged();
diff --git a/src/gui/wizard/owncloudwizard.cpp b/src/gui/wizard/owncloudwizard.cpp
index 2076136ff..8cb91e250 100644
--- a/src/gui/wizard/owncloudwizard.cpp
+++ b/src/gui/wizard/owncloudwizard.cpp
@@ -27,8 +27,10 @@
 #endif
 #include "wizard/owncloudadvancedsetuppage.h"
 #include "wizard/owncloudwizardresultpage.h"
+#ifndef NO_WEBENGINE
 #include "wizard/webviewpage.h"
 #include "wizard/flow2authcredspage.h"
+#endif
 
 #include "QProgressIndicator.h"
 
@@ -47,14 +49,18 @@ OwncloudWizard::OwncloudWizard(QWidget *parent)
     , _setupPage(new OwncloudSetupPage(this))
     , _httpCredsPage(new OwncloudHttpCredsPage(this))
     , _browserCredsPage(new OwncloudOAuthCredsPage)
+#ifndef NO_WEBENGINE
     , _flow2CredsPage(new Flow2AuthCredsPage)
+#endif
 #ifndef NO_SHIBBOLETH
     , _shibbolethCredsPage(new OwncloudShibbolethCredsPage)
 #endif
     , _advancedSetupPage(new OwncloudAdvancedSetupPage)
     , _resultPage(new OwncloudWizardResultPage)
     , _credentialsPage(nullptr)
+#ifndef NO_WEBENGINE
     , _webViewPage(new WebViewPage(this))
+#endif
     , _setupLog()
     , _registration(false)
 {
@@ -62,13 +68,17 @@ OwncloudWizard::OwncloudWizard(QWidget *parent)
     setPage(WizardCommon::Page_ServerSetup, _setupPage);
     setPage(WizardCommon::Page_HttpCreds, _httpCredsPage);
     setPage(WizardCommon::Page_OAuthCreds, _browserCredsPage);
+#ifndef NO_WEBENGINE
     setPage(WizardCommon::Page_Flow2AuthCreds, _flow2CredsPage);
+#endif
 #ifndef NO_SHIBBOLETH
     setPage(WizardCommon::Page_ShibbolethCreds, _shibbolethCredsPage);
 #endif
     setPage(WizardCommon::Page_AdvancedSetup, _advancedSetupPage);
     setPage(WizardCommon::Page_Result, _resultPage);
+#ifndef NO_WEBENGINE
     setPage(WizardCommon::Page_WebView, _webViewPage);
+#endif
 
     connect(this, &QDialog::finished, this, &OwncloudWizard::basicSetupFinished);
 
@@ -80,11 +90,15 @@ OwncloudWizard::OwncloudWizard(QWidget *parent)
     connect(_setupPage, &OwncloudSetupPage::determineAuthType, this, &OwncloudWizard::determineAuthType);
     connect(_httpCredsPage, &OwncloudHttpCredsPage::connectToOCUrl, this, &OwncloudWizard::connectToOCUrl);
     connect(_browserCredsPage, &OwncloudOAuthCredsPage::connectToOCUrl, this, &OwncloudWizard::connectToOCUrl);
+#ifndef NO_WEBENGINE
     connect(_flow2CredsPage, &Flow2AuthCredsPage::connectToOCUrl, this, &OwncloudWizard::connectToOCUrl);
+#endif
 #ifndef NO_SHIBBOLETH
     connect(_shibbolethCredsPage, &OwncloudShibbolethCredsPage::connectToOCUrl, this, &OwncloudWizard::connectToOCUrl);
 #endif
+#ifndef NO_WEBENGINE
     connect(_webViewPage, &WebViewPage::connectToOCUrl, this, &OwncloudWizard::connectToOCUrl);
+#endif
     connect(_advancedSetupPage, &OwncloudAdvancedSetupPage::createLocalAndRemoteFolders,
         this, &OwncloudWizard::createLocalAndRemoteFolders);
     connect(this, &QWizard::customButtonClicked, this, &OwncloudWizard::skipFolderConfiguration);
@@ -106,12 +120,16 @@ OwncloudWizard::OwncloudWizard(QWidget *parent)
     // Connect styleChanged events to our widgets, so they can adapt (Dark-/Light-Mode switching)
     connect(this, &OwncloudWizard::styleChanged, _setupPage, &OwncloudSetupPage::slotStyleChanged);
     connect(this, &OwncloudWizard::styleChanged, _advancedSetupPage, &OwncloudAdvancedSetupPage::slotStyleChanged);
+#ifndef NO_WEBENGINE
     connect(this, &OwncloudWizard::styleChanged, _flow2CredsPage, &Flow2AuthCredsPage::slotStyleChanged);
+#endif
 
     customizeStyle();
 
+#ifndef NO_WEBENGINE
     // allow Flow2 page to poll on window activation
     connect(this, &OwncloudWizard::onActivate, _flow2CredsPage, &Flow2AuthCredsPage::slotPollNow);
+#endif
 }
 
 void OwncloudWizard::setAccount(AccountPtr account)
@@ -180,9 +198,11 @@ void OwncloudWizard::successfulStep()
         _browserCredsPage->setConnected();
         break;
 
+#ifndef NO_WEBENGINE
     case WizardCommon::Page_Flow2AuthCreds:
         _flow2CredsPage->setConnected();
         break;
+#endif
 
 #ifndef NO_SHIBBOLETH
     case WizardCommon::Page_ShibbolethCreds:
@@ -190,9 +210,11 @@ void OwncloudWizard::successfulStep()
         break;
 #endif
 
+#ifndef NO_WEBENGINE
     case WizardCommon::Page_WebView:
         _webViewPage->setConnected();
         break;
+#endif
 
     case WizardCommon::Page_AdvancedSetup:
         _advancedSetupPage->directoriesCreated();
@@ -217,10 +239,12 @@ void OwncloudWizard::setAuthType(DetermineAuthTypeJob::AuthType type)
 #endif
         if (type == DetermineAuthTypeJob::OAuth) {
         _credentialsPage = _browserCredsPage;
+#ifndef NO_WEBENGINE
     } else if (type == DetermineAuthTypeJob::LoginFlowV2) {
         _credentialsPage = _flow2CredsPage;
     } else if (type == DetermineAuthTypeJob::WebViewFlow) {
         _credentialsPage = _webViewPage;
+#endif
     } else { // try Basic auth even for "Unknown"
         _credentialsPage = _httpCredsPage;
     }
@@ -245,7 +269,12 @@ void OwncloudWizard::slotCurrentPageChanged(int id)
     }
 
     setOption(QWizard::HaveCustomButton1, id == WizardCommon::Page_AdvancedSetup);
-    if (id == WizardCommon::Page_AdvancedSetup && (_credentialsPage == _browserCredsPage || _credentialsPage == _flow2CredsPage)) {
+    if (id == WizardCommon::Page_AdvancedSetup
+           && (_credentialsPage == _browserCredsPage
+#ifndef NO_WEBENGINE
+           || _credentialsPage == _flow2CredsPage
+#endif
+           )) {
         // For OAuth, disable the back button in the Page_AdvancedSetup because we don't want
         // to re-open the browser.
         button(QWizard::BackButton)->setEnabled(false);
diff --git a/src/gui/wizard/owncloudwizard.h b/src/gui/wizard/owncloudwizard.h
index 3cbf89f71..2f398d416 100644
--- a/src/gui/wizard/owncloudwizard.h
+++ b/src/gui/wizard/owncloudwizard.h
@@ -39,8 +39,10 @@ class OwncloudAdvancedSetupPage;
 class OwncloudWizardResultPage;
 class AbstractCredentials;
 class AbstractCredentialsWizardPage;
+#ifndef NO_WEBENGINE
 class WebViewPage;
 class Flow2AuthCredsPage;
+#endif
 
 /**
  * @brief The OwncloudWizard class
@@ -114,11 +116,15 @@ private:
 #ifndef NO_SHIBBOLETH
     OwncloudShibbolethCredsPage *_shibbolethCredsPage;
 #endif
+#ifndef NO_WEBENGINE
     Flow2AuthCredsPage *_flow2CredsPage;
+#endif
     OwncloudAdvancedSetupPage *_advancedSetupPage;
     OwncloudWizardResultPage *_resultPage;
     AbstractCredentialsWizardPage *_credentialsPage;
+#ifndef NO_WEBENGINE
     WebViewPage *_webViewPage;
+#endif
 
     QStringList _setupLog;
 
diff --git a/src/libsync/networkjobs.cpp b/src/libsync/networkjobs.cpp
index 76789a8ce..73f0bed0e 100644
--- a/src/libsync/networkjobs.cpp
+++ b/src/libsync/networkjobs.cpp
@@ -955,12 +955,20 @@ void DetermineAuthTypeJob::checkAllDone()
 
     // WebViewFlow > OAuth > Shib > Basic
     if (_account->serverVersionInt() >= Account::makeServerVersion(12, 0, 0)) {
+#ifndef NO_WEBENGINE
         result = WebViewFlow;
+#else
+        result = Basic;
+#endif
     }
 
     // LoginFlowV2 > WebViewFlow > OAuth > Shib > Basic
     if (_account->serverVersionInt() >= Account::makeServerVersion(16, 0, 0)) {
+#ifndef NO_WEBENGINE
         result = LoginFlowV2;
+#else
+        result = Basic;
+#endif
     }
 
     // If we determined that we need the webview flow (GS for example) then we switch to that
-- 
2.25.0

