# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kstars
pkgver=3.4.3
pkgrel=0
pkgdesc="Desktop planetarium"
url="https://www.kde.org/applications/education/kstars/"
arch="all"
options="!check"  # Requires FITS library.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev knotifyconfig-dev kauth-dev kconfig-dev kcrash-dev
	kdoctools-dev kwidgetsaddons-dev knewstuff-dev ki18n-dev kio-dev
	kxmlgui-dev kplotting-dev knotifications-dev eigen-dev mesa-dev
	qt5-qtwebsockets-dev libraw-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/kstars/kstars-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING:BOOL=OFF \
		-DEIGEN3_INCLUDE_DIR=/usr/include/eigen3 \
		${CMAKE_CROSSOPTS} \
		-Bbuild
	make -C build
}

check() {
	make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="47adad28cccd749a7ac3c71df4b06d59a4bdf94bacc064fe5d658dcbe3150f2e4d13aaf23e0aedea57c64c28357452d731caaf764e8c91bc67f5469166cf7ad4  kstars-3.4.3.tar.xz"
