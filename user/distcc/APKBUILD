# Contributor: Brandon Bergren <git@bdragon.rtk0.net>
# Maintainer:
pkgname=distcc
pkgver=3.3.3
pkgrel=0
pkgdesc="Distributed builds for C, C++ and Objective C"
url="https://distcc.github.io/"
arch="all"
# BadLogFile_Case depends on non-root, because distcc will NOT drop privs
# and this test checks to ensure that distcc will abort if it can't access
# its log file. Also, distccd WILL privdrop to 'distcc' and will abort if the
# user does not exist. But if we don't run tests as root, we don't need the 
# user on the build box.
options="!checkroot"
license="GPL-2.0+"
depends="python3"
makedepends="binutils-dev cmd:which popt-dev python3-dev"
subpackages="$pkgname-doc"
# These are needed if attempting to use checkroot.
#pkgusers="distcc" # distccd privdrop, see src/setuid.c
#pkggroups="distcc"
install="$pkgname.pre-install"
source="https://github.com/distcc/$pkgname/releases/download/v$pkgver/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--without-avahi \
		--disable-Werror
	make
}

check() {
	# Tests rely on invoking compiler, etc, with a normal path.
	# The default "make check" target gets confused, so we manually
	# invoke the test framework.
	make check_programs
	make PATH="$builddir:/usr/local/bin:/bin:/usr/bin" \
		TESTDISTCC_OPTS="" \
		maintainer-check-no-set-path
}

package() {
	make install DESTDIR="$pkgdir"
}

sha512sums="d5e7fc67f49ee640cef753038b5c0ebcbbac61c6ac29f20ee4736b045a89979ced765717c46383a4fadc50a4fe34e94e58e307509144414a9ca19eb4cc68a135  distcc-3.3.3.tar.gz"
