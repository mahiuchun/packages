# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=karchive
pkgver=5.74.0
pkgrel=0
pkgdesc="Framework for manipulating archive files"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.0-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen graphviz
	qt5-qttools-dev zlib-dev bzip2-dev xz-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/karchive-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c43aef33cd5cd1128637ab8ac3f7c8f26eaa27fb9f14ce0f2b5d122be2cf08f4b758fe08147c570969834600f714c1e236d1e7c05864ce1cd2dd8ca2f4e62069  karchive-5.74.0.tar.xz"
