# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdbusaddons
pkgver=5.74.0
pkgrel=0
pkgdesc="Framework for coping with D-Bus"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires running dbus-daemon
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtx11extras-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdbusaddons-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="752726ff477e0159491a4002b60720f962bc832fbf824c342cfd16194b253ef5d94963e2aaefa0daad46af73fe62b4afa888a05846123533f2a8b875be90e72a  kdbusaddons-5.74.0.tar.xz"
