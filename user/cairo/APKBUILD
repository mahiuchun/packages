# Maintainer: 
pkgname=cairo
pkgver=1.16.0
pkgrel=1
pkgdesc="A vector graphics library"
url="https://cairographics.org/"
arch="all"
options="!check"  # Recursive dependency on gtk+2.0 for check.
license="LGPL-2.0+ AND MPL-1.1"
depends=""
depends_dev="fontconfig-dev freetype-dev libxrender-dev pixman-dev
	xcb-util-dev libxext-dev $pkgname-tools"
makedepends="$depends_dev zlib-dev expat-dev glib-dev libpng-dev
	autoconf automake libtool"
subpackages="$pkgname-dev $pkgname-doc $pkgname-gobject $pkgname-tools"
# fontconfig-ultimate https://github.com/bohoomil/fontconfig-ultimate
_ultver="2016-04-23"
source="https://cairographics.org/releases/$pkgname-$pkgver.tar.xz
	fontconfig-ultimate-$_ultver.tar.gz::https://github.com/bohoomil/fontconfig-ultimate/archive/$_ultver.tar.gz
	musl-stacksize.patch
	CVE-2018-19876.patch
	"

# secfixes:
#   1.16.0-r1:
#     - CVE-2018-19876

prepare() {
	default_prepare

	# infinality
	for j in "$srcdir"/fontconfig-ultimate-$_ultver/$pkgname/*.patch; do
		msg "Applying ${j}"
		patch -p1 -i $j
	done
}

build() {
	autoreconf -vif
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--enable-ft \
		--enable-gobject \
		--enable-pdf \
		--enable-png \
		--enable-ps \
		--enable-svg \
		--enable-tee \
		--enable-x \
		--enable-xcb \
		--enable-xcb-shm \
		--enable-xlib \
		--enable-xlib-xrender \
		--disable-xlib-xcb \
		--disable-static
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

gobject() {
	pkgdesc="$pkgdesc (gobject bindings)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libcairo-gobject.so.* "$subpkgdir"/usr/lib/
}

tools() {
	pkgdesc="$pkgdesc (development tools)"
	mkdir -p "$subpkgdir"/usr/lib/cairo
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
	mv "$pkgdir"/usr/lib/cairo/libcairo-trace.* \
		"$subpkgdir"/usr/lib/cairo/
}

sha512sums="9eb27c4cf01c0b8b56f2e15e651f6d4e52c99d0005875546405b64f1132aed12fbf84727273f493d84056a13105e065009d89e94a8bfaf2be2649e232b82377f  cairo-1.16.0.tar.xz
d8185f4ec74f44c4746acf7e79bba7ff7ffd9d35bdabeb25e10b4e12825942d910931aa857f1645e5c8185bcb40a1f1ffe1e7e647428e9ea66618b2aec52fac3  fontconfig-ultimate-2016-04-23.tar.gz
86f26fe41deb5e14f553c999090d1ec1d92a534fa7984112c9a7f1d6c6a8f1b7bb735947e8ec3f26e817f56410efe8cc46c5e682f6a278d49b40a683513740e0  musl-stacksize.patch
9020c596caa54a2ac435d5dae0f121d36d3c3f34d487b9c1032665b1bd15813506adf31984e34b5dd328ee0e068de0627e1d061230758328cae4fa993c3a9209  CVE-2018-19876.patch"
