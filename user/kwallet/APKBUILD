# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kwallet
pkgver=5.74.0
pkgrel=0
pkgdesc="Secure storage system for passwords built atop Qt"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kcoreaddons-dev kconfig-dev kwindowsystem-dev
	ki18n-dev kconfigwidgets-dev kdbusaddons-dev kiconthemes-dev
	knotifications-dev kservice-dev libgcrypt-dev gpgme-dev"
makedepends="$depends_dev cmake extra-cmake-modules kdoctools-dev doxygen
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwallet-$pkgver.tar.xz
	kwallet-5.22.0-blowfish-endianness.patch"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="e18de73657c4b51c3150de88e2d764f710932527dc6e9fbfc8778e853b66e0776619fc5d8918cbf8fb6384f92d7b74836797c374d45a50223aa7d543cf743fa2  kwallet-5.74.0.tar.xz
229e001354c00832d15442b6ee2cd6062e759a560d67e9ded024735e5012aeec022f8e12ed346e090bbe2967c3bd335e5dd73b3fe5b7da93b89f1a7842411e3e  kwallet-5.22.0-blowfish-endianness.patch"
