# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=thunderbird
pkgver=68.9.0
pkgrel=0
pkgdesc="Email client from Mozilla"
url="https://www.thunderbird.net/"
arch="all"
options="!check"  # Tests disabled
license="MPL-2.0"
depends=""
# moz build system stuff
# python deps
# system-libs
# actual deps
makedepends="
	autoconf2.13 cargo cbindgen clang llvm8-dev node ncurses-dev
	perl rust cmd:which

	ncurses-dev openssl-dev

	alsa-lib-dev bzip2-dev icu-dev libevent-dev libffi-dev libpng-dev
	libjpeg-turbo-dev nspr-dev nss-dev pulseaudio-dev zlib-dev

	dbus-glib-dev fts-dev gconf-dev gtk+3.0-dev hunspell-dev
	libnotify-dev libsm-dev libxcomposite-dev libxdamage-dev
	libxrender-dev libxt-dev nasm nss-static sqlite-dev
	startup-notification-dev unzip yasm zip gtk+2.0-dev
	"
_py2ver="2.7.16"
source="https://archive.mozilla.org/pub/thunderbird/releases/$pkgver/source/thunderbird-$pkgver.source.tar.xz
	https://www.python.org/ftp/python/$_py2ver/Python-$_py2ver.tar.xz
	mozconfig

	bad-google-code.patch
	disable-gecko-profiler.patch
	fix-mutex-build.patch
	fix-seccomp-bpf.patch
	mozilla-build-arm.patch
	shut-up-warning.patch
	skia-sucks1.patch
	skia-sucks2.patch
	skia-sucks3.patch
	stackwalk-x86-ppc.patch
	webrtc-broken.patch

	thunderbird.desktop
	"
somask="liblgpllibs.so
	libmozgtk.so
	libmozsandbox.so
	libmozsqlite3.so
	libxul.so
	"
_mozappdir=/usr/lib/thunderbird
ldpath="$_mozappdir"

# secfixes:
#   68.6.0-r0:
#     - CVE-2019-20503
#     - CVE-2020-6805
#     - CVE-2020-6806
#     - CVE-2020-6807
#     - CVE-2020-6811
#     - CVE-2020-6812
#     - CVE-2020-6814
#   68.7.0-r0:
#     - CVE-2020-6819
#     - CVE-2020-6820
#     - CVE-2020-6821
#     - CVE-2020-6822
#     - CVE-2020-6825
#   68.9.0-r0:
#     - CVE-2020-6831
#     - CVE-2020-12387
#     - CVE-2020-12392
#     - CVE-2020-12395
#     - CVE-2020-12397

unpack() {
	default_unpack
	[ -z $SKIP_PYTHON ] || return 0

	msg "Killing all remaining hope for humanity and building Python 2..."
	cd "$srcdir/Python-$_py2ver"
	[ -d ../python ] && rm -r ../python

	# 19:39 <+solar> just make the firefox build process build its own py2 copy
	# 20:03 <calvin> TheWilfox: there's always violence

	sed -e 's/é/e/g' /etc/os-release > "$srcdir"/os-release
	export UNIXCONFDIR="$srcdir"

	./configure --prefix="$srcdir/python" --with-ensurepip=install
	make -j $JOBS
	# 6 tests failed:
	#    test__locale test_os test_posix test_re test_strptime test_time
	# make test
	make -j $JOBS install

	# firefox's bundled pipenv and pip aren't new enough to support
	# configurable UNIXCONFDIR
	export PATH="$srcdir/python/bin:$PATH"
	pip2 install virtualenv pipenv
}

prepare() {
	default_prepare
	cp "$srcdir"/mozconfig "$builddir"/mozconfig
	echo "ac_add_options --enable-optimize=\"$CFLAGS\"" >> "$builddir"/mozconfig
	echo "ac_add_options --host=\"$CHOST\"" >> "$builddir"/mozconfig
	echo "ac_add_options --target=\"$CTARGET\"" >> "$builddir"/mozconfig
	echo "mk_add_options MOZ_MAKE_FLAGS=\"-j$JOBS\"" >> "$builddir"/mozconfig

	case "$CARCH" in
		pmmx|x86*)
			echo "ac_add_options --disable-elf-hack" >> "$builddir"/mozconfig
			;;
		s390x)
			echo "ac_add_options --disable-startupcache" >> "$builddir"/mozconfig
			;;
	esac

	rm "$builddir"/third_party/python/virtualenv/virtualenv_support/pip*.whl
	rm "$builddir"/third_party/python/virtualenv/virtualenv_support/setuptools*.whl
	cp "$srcdir/Python-$_py2ver"/Lib/ensurepip/_bundled/*.whl \
		"$builddir/third_party/python/virtualenv/virtualenv_support"
}

build() {
	export SHELL=/bin/sh
	export BUILD_OFFICIAL=1
	export MOZILLA_OFFICIAL=1
	export USE_SHORT_LIBNAME=1
	# gcc 6
	export CXXFLAGS="-fno-delete-null-pointer-checks -fno-schedule-insns2"

	# set rpath so linker finds the libs
	export LDFLAGS="$LDFLAGS -Wl,-rpath,${_mozappdir}"

	export UNIXCONFDIR="$srcdir"

	export PATH="$srcdir/python/bin:$PATH"
	./mach build
}

run() {
	cd "$builddir"/obj-$CHOST/dist/bin
	export LD_LIBRARY_PATH=.
	./thunderbird -no-remote -profile "$builddir"/obj-$CHOST/tmp/profile-default
}

package() {
	export PATH="$srcdir/python/bin:$PATH"
	DESTDIR="$pkgdir" ./mach install

	install -m755 -d ${pkgdir}/usr/share/applications
	install -m755 -d ${pkgdir}/usr/share/pixmaps

	for png in comm/mail/branding/thunderbird/default*.png; do
		local i="${_png%.png}"
		i=${i##*/default}
		install -D -m644 "$png" \
			"$pkgdir"/usr/share/icons/hicolor/${i}x${i}/apps/thunderbird.png
	done

	install -m644 "$builddir"/comm/mail/branding/thunderbird/default256.png \
		${pkgdir}/usr/share/pixmaps/thunderbird.png
	install -m644 ${startdir}/thunderbird.desktop \
		${pkgdir}/usr/share/applications/thunderbird.desktop
}

sha512sums="891472c95ba6ff46061131504e89010da512a84b0e1dea0482e603fd4c87f11e099280a245c7dd9fc9320c48229c26602565c089d86f1a1f4271b29b6fc606f0  thunderbird-68.9.0.source.tar.xz
16e814e8dcffc707b595ca2919bd2fa3db0d15794c63d977364652c4a5b92e90e72b8c9e1cc83b5020398bd90a1b397dbdd7cb931c49f1aa4af6ef95414b43e0  Python-2.7.16.tar.xz
5519234df2934ac2f3d76c8cad7e4f0fe15cf83ea4beb32c6489d8b7839b3ebea88bdb342e0d2a9c1c7c95e9455d234b0a5aa0e73446fd8027b520f080a2bb5b  mozconfig
ace7492f4fb0523c7340fdc09c831906f74fddad93822aff367135538dacd3f56288b907f5a04f53f94c76e722ba0bab73e28d83ec12d3e672554712e6b08613  bad-google-code.patch
9c14041f0295682b8dbeb6d5b58a2f9dc0a2dc8bef995a0f7e30fa0b17c51aa0f6748f80fb8584169db7687e2eeb404dff68a09158ae56a5f24eef30685dd2b3  disable-gecko-profiler.patch
c0b2bf43206c2a5154e560ef30189a1062ae856861b39f52ce69002390ff9972d43e387bfd2bf8d2ab3cac621987bc042c8c0a8b4cf90ae05717ca7705271880  fix-mutex-build.patch
70863b985427b9653ce5e28d6064f078fb6d4ccf43dd1b68e72f97f44868fc0ce063161c39a4e77a0a1a207b7365d5dc7a7ca5e68c726825eba814f2b93e2f5d  fix-seccomp-bpf.patch
e61664bc93eadce5016a06a4d0684b34a05074f1815e88ef2613380d7b369c6fd305fb34f83b5eb18b9e3138273ea8ddcfdcb1084fdcaa922a1e5b30146a3b18  mozilla-build-arm.patch
39ddb15d1453a8412275c36fc8db3befc69dffd4a362e932d280fb7fd1190db595a2af9b468ee49e0714f5e9df6e48eb5794122a64fa9f30d689de8693acbb15  shut-up-warning.patch
e751ffab263f03d4c74feebc617e3af115b1b53cf54fe16c3acc585eec67773f37aa8de4c19599fa6478179b01439025112ef2b759aa9923c9900e7081cb65a9  skia-sucks1.patch
9152bd3e6dc446337e6a2ed602279c620aedecc796ba28e777854c4f41fcf3067f9ebd086a4b63a6b76c2e69ec599ac6435b8eeda4f7488b1c45f69113facba4  skia-sucks2.patch
7a1fa27e060b2f025eaebbd39fb5c62960b62450241437e6d057d58cef9faf1cd1a85efe3b6a37b865d686ff18e90605ebea3089b26243f2d14876c2107106a6  skia-sucks3.patch
452b47b825294779f98ed46bc1065dad76b79ff453521ef049934a120f349c84a1c863b16af1828fe053059823da9690ec917c055ae02dcc5c80c54cad732448  stackwalk-x86-ppc.patch
be68f1387aa6677875a67106e2d6a9db470c934c943056d3b53391a63034235108e41945c53957db427d9cdc59f0aa2f9e6f2f8cd862e090e512a3ab9cbcc9a8  webrtc-broken.patch
95a2b1deb4f6c90750fdd2bfe8ca0a7879a5b267965091705a6beb0a0a4b1ccad75d11df7b9885543ca4232ff704e975c6946f4c11804cb71c471e06f9576001  thunderbird.desktop"
