# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kiconthemes
pkgver=5.74.0
pkgrel=0
pkgdesc="Framework for icon theming"
url="https://www.kde.org/"
arch="all"
options="!check"  # requires X11 running
license="LGPL-2.1-only"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtsvg-dev karchive-dev ki18n-dev
	kcoreaddons-dev kconfigwidgets-dev kitemviews-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kiconthemes-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="1d34c98b2ec7c59f6960aacc222a9478212c5224eee883c4c6082dbfcba312d4fbedf164b11b5fc07d6f19a1bac65a593ed91ca65c38f998274f02c5b4ed2cfb  kiconthemes-5.74.0.tar.xz"
