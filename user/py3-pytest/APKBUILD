# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=py3-pytest
_pkgname=pytest
pkgver=5.2.0
pkgrel=0
pkgdesc="A python test library"
url="https://pypi.python.org/pypi/pytest"
arch="noarch"
options="!check"  # Cyclic dependency with itself. Passes on x86_64
# Certified net clean
license="MIT"
depends="python3 py3-atomicwrites py3-attrs py3-more-itertools
	py3-packaging py3-pluggy py3-py py3-six py3-wcwidth"
makedepends="python3-dev"
checkdepends="py3-pytest py3-hypothesis"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/p/$_pkgname/$_pkgname-$pkgver.tar.gz
	scm.patch"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	rm -f testing/test_junitxml.py
	# You would think you could get away with "python3 -m pytest" but no...
	PYTHONPATH="$builddir/build/lib:$PYTHONPATH" pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="5c766c263cdafb5e1cdb65d883ac7f5b50da14356b074448f8458e78eda8916190142dbaf7006235c02647d600e40851848e5905df17f8dff0a9fb02a90bd78d  py3-pytest-5.2.0.tar.gz
6c597ec2416017ebc0bf2e9df338234e3ab8ca187242bab2b8f411316cc04f3c29a9a939faab349b8201043c7c16ceefe614349793e2010e62f16d9e7aefa05f  scm.patch"
