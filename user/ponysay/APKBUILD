# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ponysay
pkgver=3.0.3
pkgrel=2
pkgdesc="Like cowsay, but with horses"
url="https://github.com/erkin/ponysay"
arch="noarch"
license="GPL-3.0 AND CC-BY-NC AND Custom:Other"
depends="python3"
makedepends="gzip texinfo"
subpackages="$pkgname-doc $pkgname-bash-completion:bashcomp:noarch
	$pkgname-zsh-completion:zshcomp:noarch"
source="ponysay-$pkgver.tar.gz::https://github.com/erkin/ponysay/archive/$pkgver.tar.gz"

check() {
	./dependency-test.sh
}

package() {
	python3 setup.py install --prefix=/usr --destdir="$pkgdir" --with-man \
		--freedom=sloppy --without-fish --with-custom-env-python=python3
}

bashcomp() {
	pkgdesc="Bash completions for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/bash-completion "$subpkgdir"/usr/share/
}

zshcomp() {
	pkgdesc="Z Shell completions for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel zsh"

	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/zsh "$subpkgdir"/usr/share/
}

sha512sums="d6ff905404192bdc207952a4a914458d7f25ddcfcea95763ae277a2a3bc7ab33e86a0a229c1b10ff7295b7a89d6e1b61406feefb6bdf9026f4076d0ed70dbe93  ponysay-3.0.3.tar.gz"
