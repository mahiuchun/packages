# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=udisks2
pkgver=2.9.0
pkgrel=0
pkgdesc="A Disk Manager"
url="https://www.freedesktop.org/wiki/Software/udisks"
arch="all"
license="GPL-2.0+"
depends=""
depends_dev="acl-dev gobject-introspection-dev libatasmart-dev libblockdev-dev
	libgudev-dev polkit-dev"
makedepends="$depends_dev glib-dev gtk-doc intltool linux-headers"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-libs"
source="https://github.com/storaged-project/udisks/releases/download/udisks-$pkgver/udisks-$pkgver.tar.bz2
	50-udisks2.rules
	O_CLOEXEC.patch
	"
builddir="$srcdir"/udisks-$pkgver

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	cp "$builddir"/data/org.freedesktop.UDisks2.policy.in \
		"$builddir"/data/org.freedesktop.UDisks2.policy
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -D -m644 "$srcdir"/50-udisks2.rules \
		"$pkgdir"/etc/polkit-1/rules.d/50-udisks2.rules
}

libs() {
	pkgdesc="Dynamic library to access the udisks daemon"
	license="LGPL-2.0+"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/lib*.so.* \
		"$pkgdir"/usr/lib/girepository* \
		"$subpkgdir"/usr/lib/
}

sha512sums="314355c9b0cc562b2359ea77137b3f2189c48e642c67cc9d9ed07048176967b67e78dfb3190dd160db2f92e8143a4f005bf2cc1aa814388a79201705e5297d0c  udisks-2.9.0.tar.bz2
e2b2cc10868ca341603c3403631b9962c9c776bf96f4ac2c764425363ffdbe1cd9c35233e568f84b276a1f6702b1ac5b9a14b4a62983067c44c433b58d1b7175  50-udisks2.rules
4352d63458ca51b362844d60109948ebeeecdc492a93443b391c0d75b6fff0c0af5018c8a8e441fb37bffd59c734298eaa936fcb0cd4c42ac55c1298b398b1dc  O_CLOEXEC.patch"
