# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer:
pkgname=opencv
pkgver=4.2.0
pkgrel=0
pkgdesc="Computer vision and machine learning software library"
url="https://opencv.org"
arch="all"
options="!check"  # an hour on the talos is madness
license="BSD-3-Clause"
depends=""
makedepends="cmake doxygen ffmpeg-dev gst-plugins-base-dev gtk+2.0-dev
	gtk+3.0-dev jasper-dev libdc1394-dev libgomp libgphoto2-dev
	libjpeg-turbo-dev libpng-dev libwebp-dev tiff-dev v4l-utils-dev"
subpackages="$pkgname-dev $pkgname-libs"
source="opencv-$pkgver.tar.gz::https://github.com/opencv/opencv/archive/$pkgver.tar.gz
	cmake-license.patch
	"

# secfixes:
#   4.1.1-r1:
#     - CVE-2019-16249

prepare() {
	default_prepare
	# purge 3rd party except carotene
	for i in 3rdparty/*; do
		case $i in
		*/carotene*) continue;;
		*/protobuf*) continue;;
		*/ittnotify) continue;;  # Else FTBFS on x86_64
		*/quirc)     continue;;
		esac
		rm -rf "$i"
	done
	mkdir -p build
}

build() {
	_sse=""
	if [ "$CARCH" != "x86_64" ]; then
		_sse="-DENABLE_SSE=OFF -DENABLE_SSE2=OFF"
	fi
	if [ "$CARCH" = "ppc" ]; then
		export LDFLAGS="$LDFLAGS -latomic"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DENABLE_PRECOMPILED_HEADERS=OFF \
		-DWITH_OPENMP=ON \
		-DWITH_OPENCL=ON \
		-DWITH_OPENEXR=OFF \
		-DWITH_IPP=OFF \
		$_sse \
		-Bbuild
	make -C build
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="2629d548384c81a786ab2620e089539b4b2f4ae5e1d837e5fe7f75ddadb909b65b65d0319226ae4726675f105ceb5adb88616ca6399c7660d3021ca4b9a06531  opencv-4.2.0.tar.gz
ffa6930086051c545a44d28b8e428de7faaeecf961cdee6eef007b2b01db7e5897c6f184b1059df9763c1bcd90f88b9ead710dc13b51a608f21d683f55f39bd6  cmake-license.patch"
