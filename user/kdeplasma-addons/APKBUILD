# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kdeplasma-addons
pkgver=5.18.5
pkgrel=0
pkgdesc="Extra applets and toys for KDE Plasma"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.1-only"
depends="qt5-qtquickcontrols qt5-qtquickcontrols2"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtx11extras-dev kactivities-dev kconfig-dev kconfigwidgets-dev
	kcmutils-dev kcoreaddons-dev kdoctools-dev ki18n-dev knewstuff-dev
	kross-dev krunner-dev kservice-dev kunitconversion-dev kholidays-dev
	plasma-framework-dev plasma-workspace-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/kdeplasma-addons-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild \
		.
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="7baa84335af5afc1af2cd47134863961df7a43c719bdde180754aa0b5a83975c3d5fb79435b523589751029f0fbb1751fae90db5db8e42c995c9dd24578cbeb6  kdeplasma-addons-5.18.5.tar.xz"
