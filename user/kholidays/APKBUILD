# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kholidays
pkgver=5.74.0
pkgrel=0
pkgdesc="List of national holidays for many countries"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
depends=""
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtbase-dev
	qt5-qtdeclarative-dev qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kholidays-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# Requires *actual* *locale* *support*!
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E testholidayregion
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="f35ba7c9ee42383ff629ed61dbc054656d40c7f59172bde313e897e718ee7151db79c8827582abfeb286f05c5c7b4d875c75d9790800b43c5831eb98a66e2195  kholidays-5.74.0.tar.xz"
