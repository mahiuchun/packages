# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=fuse3
_pkgname=fuse
pkgver=3.9.3
pkgrel=0
pkgdesc="The reference implementation of the Linux FUSE (Filesystem in Userspace) interface"
url="https://github.com/libfuse/libfuse"
arch="all"
options="suid !check"  # Requires fuse kernel module to be loaded
license="GPL-2.0-only AND LGPL-2.1-only AND BSD-2-Clause"
depends="fuse-common"
#checkdepends="py3-pytest cmd:which"
makedepends="linux-headers meson eudev-dev python3 py3-six py3-pluggy
	py3-attrs py3-py"
subpackages="$pkgname-dev $pkgname-doc
	$_pkgname-common:common:noarch $_pkgname-openrc:openrc:noarch"
source="https://github.com/libfuse/libfuse/releases/download/fuse-$pkgver/fuse-$pkgver.tar.xz
	fix-realpath.patch
	fuse.initd
	"
builddir="$srcdir/$_pkgname-$pkgver"

# secfixes:
#   3.2.5-r0:
#     - CVE-2018-10906

build() {
	# The examples are required for the tests to work, and aren't
	# otherwise installed

	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=release \
		-Dexamples=true \
		. output
	ninja -C output
}

check() {
	# Recommended procedure:
	# 1. modprobe fuse
	# 2. chown root and chmod u+s $builddir/output/util/fusermount3
	# 3. abuild check

	cd "$builddir"/output
	python3 -m pytest test/
}

package() {
	DESTDIR="$pkgdir" ninja -C output install
	rm -r "$pkgdir"/dev "$pkgdir"/etc/init.d

	install -Dm755 "$srcdir"/fuse.initd "$pkgdir"/etc/init.d/fuse
	install -d "$pkgdir"/etc
	cat >"$pkgdir"/etc/fuse.conf << EOF
# Set the maximum number of FUSE mounts allowed to non-root users.
# The default is 1000.
#
#mount_max = 1000

# Allow non-root users to specify the 'allow_other' or 'allow_root'
# mount options.
#
#user_allow_other
EOF
}

common() {
	pkgdesc="Common files for fuse2 and fuse3"
	mkdir -p "$subpkgdir"/etc
	mv "$pkgdir"/etc/fuse.conf "$subpkgdir"/etc
}

openrc() {
	default_openrc
	install_if="openrc fuse-common=$pkgver-r$pkgrel"
}

sha512sums="e4a4bfbc276b1d676211d93878b487c5946f54e8811e929fad4ff32de1f64e0c4e762d33c43e3a04afae538f41efe8d29221f45340ccd74f0055f644802f3e26  fuse-3.9.3.tar.xz
1a9e1d1e8a7b0778ffde328e4322c73b5d57ec98d52767c846d755cce861ab27989823a75b6c5f994432ddb77fa351dfa4a8f948c9467c5f7d5f471e4608358b  fix-realpath.patch
7f6a503ef23cfa8b809c544375c2d83ad56525269b48ad1a7dff0ce36f4bf2f2a3fafed9dc70a71ff6281b261db5f01829e16c06f041921a5d8c8d715a04a8c1  fuse.initd"
