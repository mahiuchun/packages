# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kfilemetadata
pkgver=5.74.0
pkgrel=0
pkgdesc="File metadata extraction framework"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="catdoc"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qtmultimedia-dev qt5-qttools-dev exiv2-dev karchive-dev ki18n-dev
	poppler-dev poppler-qt5-dev taglib-dev kcoreaddons-dev ffmpeg-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kfilemetadata-$pkgver.tar.xz
	test-mimetype.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="7b07d051f1bd339af6e24450c7ad325685d2ba1e4eda2adb14a25b91536f80d3a045d77501b143a41aa54012b043aed3a324f7c24fef60e72e63f42830034010  kfilemetadata-5.74.0.tar.xz
ddbf9eb485b9f20c87284434bb705f2f401c77f6eaa2e611ee0cb4f15ef585c47c8dd39eb2f38e347f80db5e2bc0362861ed90beb8e09d9882172a899e473bff  test-mimetype.patch"
