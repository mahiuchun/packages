# Maintainer: Max Rees <maxcrees@me.com>
pkgname=mumble
pkgver=1.3.3
pkgrel=0
pkgdesc="Low-latency, high-quality voice chat (client)"
url="https://www.mumble.info/"
arch="all"
license="BSD-3-Clause AND BSD-2-Clause AND MIT AND GPL-2.0+"
depends="qt5-qtbase-sqlite"
makedepends="alsa-lib-dev avahi-dev boost-dev cmd:which libcap-dev
	libsndfile-dev libxi-dev opus-dev protobuf-dev pulseaudio-dev
	qt5-qtbase-dev qt5-qtsvg-dev qt5-qttools-dev speech-dispatcher-dev
	speex-dev speexdsp-dev"
subpackages="$pkgname-doc $pkgname-lang murmur murmur-openrc:openrc:noarch"
install="murmur.pre-install"
pkgusers="murmur"
pkggroups="murmur"
source="https://github.com/mumble-voip/mumble/releases/download/$pkgver/mumble-$pkgver.tar.gz
	murmur.initd
	default-config.patch
	tests-networking.patch
	"

_qmake() {
	qmake -recursive "$@" \
		CONFIG+="\
		bundled-celt \
		no-bundled-opus \
		no-bundled-speex \
		no-embed-qt-translations \
		no-g15 \
		no-ice \
		no-pch \
		no-rnnoise \
		no-update \
		" \
		DEFINES+="PLUGIN_PATH=/usr/lib/mumble"
}

build() {
	_qmake main.pro
	make release
}

check() {
	cd src/tests
	_qmake tests.pro
	make check
}

package() {
	_ver=${pkgver%_rc*}
	_maj="${_ver%%.*}"
	_min="${_ver#*.}"
	_min="${_min%%.*}"

	# Binaries
	install -Dm755 release/mumble \
		"$pkgdir"/usr/bin/mumble
	install -Dm755 release/murmurd \
		"$pkgdir"/usr/bin/murmurd

	# Libraries & plugins
	install -Dm755 release/libmumble.so.$_ver \
		"$pkgdir"/usr/lib/mumble/libmumble.so.$_ver
	for lib in libmumble.so libmumble.so.$_maj libmumble.so.$_maj.$_min; do
		ln -s libmumble.so.$_ver \
			"$pkgdir"/usr/lib/mumble/$lib
	done
	install -Dm755 release/plugins/liblink.so \
		"$pkgdir"/usr/lib/mumble/liblink.so
	install -Dm755 release/libcelt* \
		"$pkgdir"/usr/lib/mumble/

	# Desktop files
	install -Dm644 scripts/mumble.desktop \
		"$pkgdir"/usr/share/applications/mumble.desktop
	install -Dm644 icons/mumble.svg \
		"$pkgdir"/usr/share/icons/hicolor/scalable/apps/mumble.svg

	# Man pages
	install -Dm644 -t "$pkgdir"/usr/share/man/man1 \
		man/mumble.1 \
		man/murmurd.1

	# Translations
	install -Dm644 -t "$pkgdir"/usr/share/mumble/translations \
		src/mumble/*.qm

	# OpenRC
	install -Dm755 "$srcdir"/murmur.initd \
		"$pkgdir"/etc/init.d/murmur
}

lang() {
	pkgdesc="Languages for package mumble"
	install_if="$pkgname=$pkgver-r$pkgrel lang"

	cd "$builddir"
	mkdir -p "$subpkgdir"/usr/share/mumble
	mv "$pkgdir"/usr/share/mumble/translations \
		"$subpkgdir"/usr/share/mumble
}

murmur() {
	pkgdesc="Low latency, high-quality voice chat (server)"

	cd "$builddir"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/murmurd \
		"$subpkgdir"/usr/bin
	install -o murmur -g murmur -dm750 \
		"$subpkgdir"/var/lib/murmur
	install -g murmur -Dm640 scripts/murmur.ini \
		"$subpkgdir"/etc/murmur.ini
}

openrc() {
	default_openrc
	install_if="openrc murmur=$pkgver-r$pkgrel"
	pkgdesc="Low latency, high-quality voice chat (server OpenRC init scripts)"
}

sha512sums="be4c6d4de82a1059bf30d4c7e3c44e41e4bb50dc4a811b7d0def808c52059ff7bcccf65140db940f18cc1bb66d58ea4dab23ba5dcfae3b8b904866751f32edb3  mumble-1.3.3.tar.gz
59283687466c9ab460017c2191c731e63203baaa6a4a3d30b42075eb8597bafeb2d3494367ab64c785ee37c7b916ea101daf22b68bdfa27844b97e18cb1d71c0  murmur.initd
68c4c81a55663305d1525eb5d43e0b456e54f007ee327d45bf63572c59282edd88650ede7931644a9762a40c2f7e730b338b4900ae5b4da11b944b3af01c5387  default-config.patch
e89c20b39fdb24982153b046ffe41688d6a854eee593205535155d55e49b7e534f6cec14325108a7ebd2550a6043479b01139d7bf900840bcf63188625bca304  tests-networking.patch"
