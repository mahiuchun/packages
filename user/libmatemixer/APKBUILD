# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=libmatemixer
pkgver=1.24.1
pkgrel=0
pkgdesc="Sound mixer library for the MATE desktop environment"
url="https://mate-desktop.org"
arch="all"
license="LGPL-2.0+"
depends=""
makedepends="alsa-lib-dev intltool pulseaudio-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang
	$pkgname-alsa $pkgname-pulse"
source="https://pub.mate-desktop.org/releases/${pkgver%.*}/libmatemixer-$pkgver.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

alsa() {
	pkgdesc="$pkgdesc (ALSA backend)"
	install_if="$pkgname=$pkgver-r$pkgrel alsa-lib"
	mkdir -p "$subpkgdir"/usr/lib/$pkgname
	mv "$pkgdir"/usr/lib/$pkgname/$pkgname-alsa.so "$subpkgdir"/usr/lib/$pkgname
}

pulse() {
	pkgdesc="$pkgdesc (PulseAudio backend)"
	install_if="$pkgname=$pkgver-r$pkgrel pulseaudio"
	mkdir -p "$subpkgdir"/usr/lib/$pkgname
	mv "$pkgdir"/usr/lib/$pkgname/$pkgname-pulse.so "$subpkgdir"/usr/lib/$pkgname
}

sha512sums="c4620d70c66196521296d2d4197a6e025629faf1324e726df7dd7b0e0444ee17b83355c5059ec17ef36f9f15e6ab3bf3bb82a152b087a11e929d7749c1e1d4e6  libmatemixer-1.24.1.tar.xz"
