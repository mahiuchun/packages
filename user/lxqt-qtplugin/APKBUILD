# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=lxqt-qtplugin
pkgver=0.15.1
pkgrel=0
pkgdesc="Qt plugin for platform integration with LXQt"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.7.0 qt5-qttools-dev
	libfm-qt-dev>=${pkgver%.*}.0 liblxqt-dev>=${pkgver%.*}.0
	libdbusmenu-qt-dev"
source="https://github.com/lxqt/lxqt-qtplugin/releases/download/$pkgver/lxqt-qtplugin-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="6fc19cf5f763d7879e5a052c5638b5b396f659f4e199493c8b87cdb661c910921ecda865de2900a047c96dd63c3ef6dbfa486d1dbe0257c26984a25a139dce2f  lxqt-qtplugin-0.15.1.tar.xz"
