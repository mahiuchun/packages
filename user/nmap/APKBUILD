# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=nmap
pkgver=7.80
pkgrel=0
pkgdesc="A network exploration tool and security/port scanner"
url="https://nmap.org/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="linux-headers openssl-dev libpcap-dev pcre-dev zlib-dev
	libssh2-dev lua5.3-dev"
subpackages="
	$pkgname-doc
	$pkgname-scripts::noarch
	$pkgname-nselibs::noarch
	$pkgname-nping
	$pkgname-ncat
	$pkgname-ncat-doc:ncat_doc
	netcat::noarch"
source="https://nmap.org/dist/$pkgname-$pkgver.tar.bz2
	nmap-7.80-fix_addrset.patch"

# secfixes:
#   7.80-r0:
#     - CVE-2018-15173

prepare() {
	default_prepare
	update_config_sub
}

build() {
        # zenmap and ndiff require python 2
	export CFLAGS=-g3
	export CPPFLAGS=-g3
	export CXXFLAGS=-g3
	LDFLAGS="$LDFLAGS -L/usr/lib/lua5.3" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--without-zenmap \
		--without-ndiff \
		--with-openssl=yes \
		--with-libpcap=yes \
		--with-libpcre=yes \
		--with-libz=yes \
		--with-libssh2=yes \
		--with-liblua=yes
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 COPYING ${pkgdir}/usr/share/licenses/${pkgname}/LICENSE
}

scripts() {
	depends="$pkgname-nselibs"
	pkgdesc="$pkgdesc (scripts)"

	mkdir -p "$subpkgdir"/usr/share/nmap/
	mv "$pkgdir"/usr/share/nmap/scripts \
		"$subpkgdir"/usr/share/nmap/
}

nselibs() {
	pkgdesc="$pkgdesc (nselibs)"

	mkdir -p "$subpkgdir"/usr/share/nmap/
	mv "$pkgdir"/usr/share/nmap/nse_main.lua \
		"$pkgdir"/usr/share/nmap/nselib \
		"$subpkgdir"/usr/share/nmap/
}

ncat() {
	pkgdesc="$pkgdesc (ncat tool)"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/ncat "$subpkgdir"/usr/bin
}

ncat_doc() {
        pkgdesc="ncat utility (docs)"
	install_if="nmap-ncat=$pkgver-r$pkgrel docs"
	mkdir -p "$subpkgdir"/usr/share/man/man1
	mv "${pkgdir}-doc"/usr/share/man/man1/ncat.1.gz \
		"$subpkgdir"/usr/share/man/man1
}

nping() {
	pkgdesc="$pkgdesc (nping tool)"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/nping "$subpkgdir"/usr/bin
}

netcat() {
	pkgdesc="Symlinks for netcat and nc to ncat"
	depends="$pkgname-ncat"
	mkdir -p "$subpkgdir"/usr/bin
	ln -s ncat "$subpkgdir"/usr/bin/netcat
	ln -s ncat "$subpkgdir"/usr/bin/nc
}

sha512sums="d4384d3ebf4f3abf3588eed5433f733874ecdceb9342a718dc36db19634b0cc819d73399974eb0a9a9c9dd9e5c88473e07644ec91db28b0c072552b54430be6b  nmap-7.80.tar.bz2
e079c07716bc847b44cb5ba0c1f71fe7d95e786c7a18dad7927ca29e6f2e20fce3674d939335db038e509755945d1db05a1746b508ada3df011fafb890ab9033  nmap-7.80-fix_addrset.patch"
