# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=rsibreak
pkgver=0.12.13
pkgrel=0
pkgdesc="Helps you avoid wrist injury by telling you when to stop for a rest"
url="https://www.kde.org/applications/utilities/rsibreak/"
arch="all"
options="!check"  # All tests require X11.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kconfig-dev kcrash-dev
	kconfigwidgets-dev kdbusaddons-dev kdoctools-dev kiconthemes-dev
	ki18n-dev kidletime-dev knotifications-dev knotifyconfig-dev
	ktextwidgets-dev kxmlgui-dev kwindowsystem-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/rsibreak/0.12/rsibreak-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="dc1411221828afab1c8f3ee89238be44d9fd3784cb5cce4f5a50ce063e14d1683257bffa1b24509d7009e74d0be16f1c788754690b503490fb8be1bedc9657fd  rsibreak-0.12.13.tar.xz"
