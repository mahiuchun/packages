# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kanagram
pkgver=20.08.1
pkgrel=0
pkgdesc="Letter order (anagram) game"
url="https://www.kde.org/applications/education/kanagram/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	ki18n-dev kcrash-dev sonnet-dev kconfig-dev kconfigwidgets-dev kio-dev
	kcoreaddons-dev kdeclarative-dev kdoctools-dev knewstuff-dev
	libkeduvocdocument-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kanagram-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="82a90e3ef602ec90755f82292b7dbafbbef91698c7279a7b20de66b5fc2bfa2ac5722d4f4852f0c919ca9f6b6b3db069649efe33b9d2c3df95856deeca11bab8  kanagram-20.08.1.tar.xz"
