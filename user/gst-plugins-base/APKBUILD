# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gst-plugins-base
pkgver=1.16.2
pkgrel=0
pkgdesc="GStreamer multimedia framework - Base plugins"
url="https://gstreamer.freedesktop.org/"
arch="all"
options="!check"  # fails overlaycomposition on ppc64
license="GPL LGPL"
depends=""
checkdepends="orc-compiler"
makedepends="alsa-lib-dev cdparanoia-dev expat-dev glib-dev
	gobject-introspection-dev gstreamer-dev libice-dev libogg-dev libsm-dev
	libtheora-dev libvorbis-dev libx11-dev libxt-dev libxv-dev mesa-dev
	opus-dev orc-dev pango-dev perl cmd:which !gst-plugins-base"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
replaces="gst-plugins-base1"
source="https://gstreamer.freedesktop.org/src/gst-plugins-base/gst-plugins-base-$pkgver.tar.xz"
ldpath="/usr/lib/gstreamer-1.0"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--disable-static \
		--disable-experimental \
		--disable-fatal-warnings \
		--with-default-audiosink=alsasink \
		--enable-introspection \
		--with-package-name="GStreamer Base Plugins (${DISTRO_NAME:-Adélie Linux})" \
		--with-package-origin="${DISTRO_URL:-https://www.adelielinux.org/}"
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

doc() {
	default_doc
	replaces="${pkgname}1-doc"
}

sha512sums="f28e71bba8ba25d4f18ba3a196f057721151f1ebf1309d808bd6771a3f9a68facfa1970dc4353b6f2fd1e8945edf5272854d328ea11ef399544f8b330f754a42  gst-plugins-base-1.16.2.tar.xz"
