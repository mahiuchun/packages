# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kinit
pkgver=5.74.0
pkgrel=0
pkgdesc="KDE initialisation routines"
url="https://www.kde.org/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev libx11-dev libxext-dev
	libice-dev libxcb-dev kdoctools-dev python3 libcap-dev kservice-dev
	kio-dev ki18n-dev kwindowsystem-dev kcrash-dev kconfig-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kinit-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a75e972fc2b4d967c3a7600260b7aef0754b40e0b5ae8803ee29c2186736222fd88b22349431393a9731d59ef7ed9bb184f413a39df82ed735d82130042423a8  kinit-5.74.0.tar.xz"
