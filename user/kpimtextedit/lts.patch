From 1f3695894e9ca44297dc4004448ece8b3647525b Mon Sep 17 00:00:00 2001
From: Laurent Montel <montel@kde.org>
Date: Tue, 26 May 2020 07:37:01 +0200
Subject: [PATCH] don't depend against qt5.13

---
 .../commonwidget/texteditfindbarbase.h        | 10 ++++++++++
 .../commonwidget/textfindreplacewidget.cpp    | 19 +++++++++++++++++++
 .../commonwidget/textfindreplacewidget.h      |  4 ++++
 .../plaintexteditor/plaintexteditfindbar.cpp  | 11 +++++++++++
 .../plaintexteditor/plaintexteditfindbar.h    |  4 ++++
 .../richtexteditor/richtexteditfindbar.cpp    | 11 +++++++++++
 .../richtexteditor/richtexteditfindbar.h      |  4 ++++
 7 files changed, 63 insertions(+)

diff --git a/src/texteditor/commonwidget/texteditfindbarbase.h b/src/texteditor/commonwidget/texteditfindbarbase.h
index ae049ad..b7ac825 100644
--- a/src/texteditor/commonwidget/texteditfindbarbase.h
+++ b/src/texteditor/commonwidget/texteditfindbarbase.h
@@ -23,7 +23,9 @@
 #include "kpimtextedit_export.h"
 #include <QWidget>
 #include <QTextDocument>
+#if QT_VERSION >= QT_VERSION_CHECK(5, 13, 0)
 #include <QRegularExpression>
+#endif
 namespace KPIMTextEdit {
 class TextFindWidget;
 class TextReplaceWidget;
@@ -56,7 +58,11 @@ protected:
     virtual Q_REQUIRED_RESULT bool viewIsReadOnly() const = 0;
     virtual Q_REQUIRED_RESULT bool documentIsEmpty() const = 0;
     virtual Q_REQUIRED_RESULT bool searchInDocument(const QString &text, QTextDocument::FindFlags searchOptions) = 0;
+#if QT_VERSION < QT_VERSION_CHECK(5, 13, 0)
+    virtual Q_REQUIRED_RESULT bool searchInDocument(const QRegExp &regExp, QTextDocument::FindFlags searchOptions) = 0;
+#else
     virtual Q_REQUIRED_RESULT bool searchInDocument(const QRegularExpression &regExp, QTextDocument::FindFlags searchOptions) = 0;
+#endif
     virtual void autoSearchMoveCursor() = 0;
 
     bool event(QEvent *e) override;
@@ -83,7 +89,11 @@ private Q_SLOTS:
 
 protected:
     QString mLastSearchStr;
+#if QT_VERSION < QT_VERSION_CHECK(5, 13, 0)
+    QRegExp mLastSearchRegExp;
+#else
     QRegularExpression mLastSearchRegExp;
+#endif
     TextFindWidget *mFindWidget = nullptr;
     TextReplaceWidget *mReplaceWidget = nullptr;
     bool mHideWhenClose = true;
diff --git a/src/texteditor/commonwidget/textfindreplacewidget.cpp b/src/texteditor/commonwidget/textfindreplacewidget.cpp
index 0e208d1..99d4bf5 100644
--- a/src/texteditor/commonwidget/textfindreplacewidget.cpp
+++ b/src/texteditor/commonwidget/textfindreplacewidget.cpp
@@ -174,6 +174,24 @@ QString TextFindWidget::searchText() const
     return mSearch->text();
 }
 
+#if QT_VERSION < QT_VERSION_CHECK(5, 13, 0)
+QRegExp TextFindWidget::searchRegExp() const
+{
+    QRegExp reg;
+    if (mCaseSensitiveAct->isChecked()) {
+        reg.setCaseSensitivity(Qt::CaseSensitive);
+    } else {
+        reg.setCaseSensitivity(Qt::CaseInsensitive);
+    }
+    QString searchTextString = mSearch->text();
+    if (mWholeWordAct->isChecked()) {
+        searchTextString = QLatin1String("\\b") + searchTextString + QLatin1String("\\b");
+    }
+    reg.setPattern(searchTextString);
+    return reg;
+}
+
+#else
 QRegularExpression TextFindWidget::searchRegExp() const
 {
     QRegularExpression reg;
@@ -188,6 +206,7 @@ QRegularExpression TextFindWidget::searchRegExp() const
     return reg;
 }
 
+#endif
 QTextDocument::FindFlags TextFindWidget::searchOptions() const
 {
     QTextDocument::FindFlags opt = {};
diff --git a/src/texteditor/commonwidget/textfindreplacewidget.h b/src/texteditor/commonwidget/textfindreplacewidget.h
index c918ab4..13926d8 100644
--- a/src/texteditor/commonwidget/textfindreplacewidget.h
+++ b/src/texteditor/commonwidget/textfindreplacewidget.h
@@ -41,7 +41,11 @@ public:
     void setFoundMatch(bool match);
 
     Q_REQUIRED_RESULT bool isRegularExpression() const;
+#if QT_VERSION < QT_VERSION_CHECK(5, 13, 0)
+    Q_REQUIRED_RESULT QRegExp searchRegExp() const;
+#else
     Q_REQUIRED_RESULT QRegularExpression searchRegExp() const;
+#endif
 
     Q_REQUIRED_RESULT QString searchText() const;
 
diff --git a/src/texteditor/plaintexteditor/plaintexteditfindbar.cpp b/src/texteditor/plaintexteditor/plaintexteditfindbar.cpp
index 9f8f2c6..4ad7013 100644
--- a/src/texteditor/plaintexteditor/plaintexteditfindbar.cpp
+++ b/src/texteditor/plaintexteditor/plaintexteditfindbar.cpp
@@ -75,6 +75,15 @@ bool PlainTextEditFindBar::searchInDocument(const QString &text, QTextDocument::
     return found;
 }
 
+#if QT_VERSION < QT_VERSION_CHECK(5, 13, 0)
+bool PlainTextEditFindBar::searchInDocument(const QRegExp &regExp, QTextDocument::FindFlags searchOptions)
+{
+    const bool found = d->mView->find(regExp, searchOptions);
+    mFindWidget->setFoundMatch(found);
+    return found;
+}
+
+#else
 bool PlainTextEditFindBar::searchInDocument(const QRegularExpression &regExp, QTextDocument::FindFlags searchOptions)
 {
     const bool found = d->mView->find(regExp, searchOptions);
@@ -82,6 +91,8 @@ bool PlainTextEditFindBar::searchInDocument(const QRegularExpression &regExp, QT
     return found;
 }
 
+#endif
+
 void PlainTextEditFindBar::autoSearchMoveCursor()
 {
     QTextCursor cursor = d->mView->textCursor();
diff --git a/src/texteditor/plaintexteditor/plaintexteditfindbar.h b/src/texteditor/plaintexteditor/plaintexteditfindbar.h
index 5f19429..cf7ed57 100644
--- a/src/texteditor/plaintexteditor/plaintexteditfindbar.h
+++ b/src/texteditor/plaintexteditor/plaintexteditfindbar.h
@@ -43,7 +43,11 @@ protected:
     Q_REQUIRED_RESULT bool viewIsReadOnly() const override;
     Q_REQUIRED_RESULT bool documentIsEmpty() const override;
     Q_REQUIRED_RESULT bool searchInDocument(const QString &text, QTextDocument::FindFlags searchOptions) override;
+#if QT_VERSION < QT_VERSION_CHECK(5, 13, 0)
+    Q_REQUIRED_RESULT bool searchInDocument(const QRegExp &regExp, QTextDocument::FindFlags searchOptions) override;
+#else
     Q_REQUIRED_RESULT bool searchInDocument(const QRegularExpression &regExp, QTextDocument::FindFlags searchOptions) override;
+#endif
     void autoSearchMoveCursor() override;
 
 public Q_SLOTS:
diff --git a/src/texteditor/richtexteditor/richtexteditfindbar.cpp b/src/texteditor/richtexteditor/richtexteditfindbar.cpp
index 2e76ba4..3c5ad36 100644
--- a/src/texteditor/richtexteditor/richtexteditfindbar.cpp
+++ b/src/texteditor/richtexteditor/richtexteditfindbar.cpp
@@ -74,6 +74,15 @@ bool RichTextEditFindBar::searchInDocument(const QString &text, QTextDocument::F
     return found;
 }
 
+#if QT_VERSION < QT_VERSION_CHECK(5, 13, 0)
+bool RichTextEditFindBar::searchInDocument(const QRegExp &regExp, QTextDocument::FindFlags searchOptions)
+{
+    const bool found = d->mView->find(regExp, searchOptions);
+    mFindWidget->setFoundMatch(found);
+    return found;
+}
+
+#else
 bool RichTextEditFindBar::searchInDocument(const QRegularExpression &regExp, QTextDocument::FindFlags searchOptions)
 {
     const bool found = d->mView->find(regExp, searchOptions);
@@ -81,6 +90,8 @@ bool RichTextEditFindBar::searchInDocument(const QRegularExpression &regExp, QTe
     return found;
 }
 
+#endif
+
 void RichTextEditFindBar::autoSearchMoveCursor()
 {
     QTextCursor cursor = d->mView->textCursor();
diff --git a/src/texteditor/richtexteditor/richtexteditfindbar.h b/src/texteditor/richtexteditor/richtexteditfindbar.h
index 5341c63..d0041e3 100644
--- a/src/texteditor/richtexteditor/richtexteditfindbar.h
+++ b/src/texteditor/richtexteditor/richtexteditfindbar.h
@@ -42,7 +42,11 @@ protected:
     Q_REQUIRED_RESULT bool viewIsReadOnly() const override;
     Q_REQUIRED_RESULT bool documentIsEmpty() const override;
     Q_REQUIRED_RESULT bool searchInDocument(const QString &text, QTextDocument::FindFlags searchOptions) override;
+#if QT_VERSION < QT_VERSION_CHECK(5, 13, 0)
+    Q_REQUIRED_RESULT bool searchInDocument(const QRegExp &regExp, QTextDocument::FindFlags searchOptions) override;
+#else
     Q_REQUIRED_RESULT bool searchInDocument(const QRegularExpression &regExp, QTextDocument::FindFlags searchOptions) override;
+#endif
     void autoSearchMoveCursor() override;
 
 public Q_SLOTS:
-- 
GitLab

--- kpimtextedit-20.08.1/CMakeLists.txt.old	2020-09-01 06:34:13.000000000 +0000
+++ kpimtextedit-20.08.1/CMakeLists.txt	2020-09-17 16:33:36.113343815 +0000
@@ -37,7 +37,7 @@
     SOVERSION 5
 )
 
-set(QT_REQUIRED_VERSION "5.13.0")
+set(QT_REQUIRED_VERSION "5.12.9")
 find_package(Qt5 ${QT_REQUIRED_VERSION} CONFIG REQUIRED COMPONENTS Widgets)
 
 ########### Find packages ###########
