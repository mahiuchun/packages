# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kpimtextedit
pkgver=20.08.1
pkgrel=0
pkgdesc="Text editor for Personal Information Managers (PIMs)"
url="https://pim.kde.org/"
arch="all"
options="!check"  # Test suite requires X11 running.
license="LGPL-2.1+ AND GPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev kwidgetsaddons-dev syntax-highlighting-dev
	ktextwidgets-dev sonnet-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev ki18n-dev
	grantlee-dev kcodecs-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kdesignerplugin-dev kemoticons-dev kiconthemes-dev kio-dev kxmlgui-dev
	qt5-qtspeech-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpimtextedit-$pkgver.tar.xz
	lts.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="778a5d51fcd37f1003437e2929990ca0b0aadd80855798184e379d1d2ae09e6f1ba7a23249e36f96d076782f8121284faf2b9f2b35a339e97c6f176d4ff8e229  kpimtextedit-20.08.1.tar.xz
84832b4ef9f04f8de9e1498bcba7e4856781617931d350786b1624d8a5c394b3b8790670c529fc3010e94e2c53b23d99136fa82d76fdc8697d5e62fcfb5dfe96  lts.patch"
