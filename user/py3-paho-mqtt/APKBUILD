# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=py3-paho-mqtt
_pkgname=paho.mqtt.python
pkgver=1.5.0
pkgrel=0
pkgdesc="MQTT version 3.1.1 client class for Python"
url="https://www.eclipse.org/paho/"
# Certified net clean
arch="noarch"
license="EPL-1.0 AND EDL-1.0"
depends="python3"
checkdepends="py3-pytest"
makedepends=""
# Use GitHub tarball since PyPI doesn't include tests
source="$pkgname-$pkgver.tar.gz::https://github.com/eclipse/paho.mqtt.python/archive/v$pkgver.tar.gz
	setup.patch"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$builddir"/src pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="7e52180bf1783ee1f39aa5248730e82fae47fe3db7c4579b0b7207a29f5337c7c0af58d58aac1c265b1ed91fa9c240187d5e3005f55c6e28623e6cbc36750294  py3-paho-mqtt-1.5.0.tar.gz
690f18379f1d920c9eb63a14dfa1331e8400d95db5a6038df85aaa78756d965a303a5b00877615b1546cefb804994dec99c4067564d899421aceb0e0ad3c3e68  setup.patch"
