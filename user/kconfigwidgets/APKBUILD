# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kconfigwidgets
pkgver=5.74.0
pkgrel=0
pkgdesc="Framework providing widgets for software configuration"
url="https://www.kde.org/"
arch="all"
options="!check"  # All tests require X11
license="LGPL-2.1-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kauth-dev kcodecs-dev kconfig-dev kguiaddons-dev
	ki18n-dev kwidgetsaddons-dev kdoctools-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kconfigwidgets-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c51f005494aed3587a3f39a8582d5cb3e1f9e4c6da0074d9e9ad78038e63663278c891231c6358ca38ee9b41e00fe13da2bf0719b3213507eae8e5da35275e76  kconfigwidgets-5.74.0.tar.xz"
