# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kactivities-stats
pkgver=5.74.0
pkgrel=0
pkgdesc="Gather statistics about KDE activities"
url="https://api.kde.org/frameworks/kactivities/html/index.html"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
depends_dev="kactivities-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtbase-dev boost-dev
	qt5-qtdeclarative-dev qt5-qttools-dev doxygen graphviz kconfig-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kactivities-stats-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="0caa8afb52b7643cf21a6d3fdaa3e987b632f1414e9888fc174623fe8b646c3eef4a8f57a2d0d22f7d726e9952a074550d1f35fb0f65cc5d061222746a6dbbac  kactivities-stats-5.74.0.tar.xz"
