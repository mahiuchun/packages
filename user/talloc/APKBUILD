# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=talloc
pkgver=2.3.1
pkgrel=0
pkgdesc="Memory pool management library"
url="https://talloc.samba.org"
arch="all"
license="LGPL-3.0+ AND GPL-3.0+ AND ISC AND LGPL-2.1+ AND BSD-3-Clause AND PostgreSQL"
depends=""
makedepends="cmd:which docbook-xsl libxslt python3-dev"
replaces="samba-common"
subpackages="$pkgname-dev py3-$pkgname:py3 $pkgname-doc"
source="https://samba.org/ftp/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	PYTHON=python3 ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--bundled-libraries=NONE \
		--builtin-libraries=replace \
		--disable-rpath \
		--disable-rpath-install
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

py3() {
	pkgdesc="Python 3 binding for libtalloc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libpytalloc-util.cpython* \
		"$pkgdir"/usr/lib/python3* "$subpkgdir"/usr/lib/
}

sha512sums="064fc39a9aaace6e0209f3251c8ff198d8a318b4cf4198006ff9892ca6e15e7d817b2fda43e0444fbbf04d2c3e70d06523dff5d57cbb796d27317ef4759e062e  talloc-2.3.1.tar.gz"
