# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=partitionmanager
pkgver=4.1.0
pkgrel=0
pkgdesc="Qt-based partition manager"
url="https://www.kde.org/applications/system/partitionmanager"
arch="all"
license="GPL-3.0+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kconfig-dev
	kconfigwidgets-dev kcoreaddons-dev kcrash-dev kdoctools-dev ki18n-dev
	kiconthemes-dev kio-dev kjobwidgets-dev kservice-dev kwidgetsaddons-dev
	kxmlgui-dev kpmcore-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/partitionmanager/$pkgver/src/partitionmanager-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="63f77c68f0468e6b17da33bf52824e2858b43f33cb46f8ff7069386d56a154c86f5f3ec20113c472d567a790ba0d8593c2ecb44d9c0d4e1614dcdc570b072761  partitionmanager-4.1.0.tar.xz"
