# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=killbots
pkgver=20.08.1
pkgrel=0
pkgdesc="Simple game of evading killer robots"
url="https://www.kde.org/applications/games/killbots/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kcoreaddons-dev
	kconfig-dev kcrash-dev kwidgetsaddons-dev ki18n-dev kconfigwidgets-dev
	kxmlgui-dev kcompletion-dev kdoctools-dev kdbusaddons-dev
	libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/killbots-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="2b9315b89f73f81438f887b1231737b4b945a3296547df004300d48bbc5f7f8b98c251a8f8680a9db41469921e9f658fe6bce0b7711de92ab809ccc68793cd91  killbots-20.08.1.tar.xz"
