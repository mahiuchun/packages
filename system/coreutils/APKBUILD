# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=coreutils
pkgver=8.32
pkgrel=0
pkgdesc="Basic file, shell, and text manipulation utilities"
url="https://www.gnu.org/software/coreutils/"
arch="all"
license="GPL-3.0+"
makedepends="bash acl-dev attr-dev"
[ "${CBUILD}" = "${CHOST}" ] && makedepends="$makedepends perl gettext-tiny
						gettext-tiny-dev utmps-dev"
subpackages="$pkgname-doc"
[ "${CBUILD}" = "${CHOST}" ] && subpackages="$subpackages $pkgname-lang"
install="$pkgname.post-deinstall"
source="https://ftp.gnu.org/gnu/coreutils/$pkgname-$pkgver.tar.xz
	cp-dont-delete.patch
	disable-csplit-io-err-test.patch
	disable-mbrtowc-test.patch
	gnulib-tests-dont-require-gpg-passphrase.patch
	gnulib-test-fixes.patch
	localename-test-fix.patch
	ls-removed-dirs.patch
	test-df-symlink-bindmount.patch
	"
[ "${CBUILD}" != "${CHOST}" ] && source="$source
	051_all_coreutils-mangen.patch
	"

build() {
	LIBS="-lutmps -lskarnet" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/ \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--datarootdir=/usr/share \
		--libexecdir=/usr/libexec \
		--enable-nls \
		--without-gmp \
		--enable-no-install-program=groups,hostname,su,kill,uptime \
		--enable-install-program=arch \
		--with-packager="Adélie" \
		--with-packager-bug-reports="https://bts.adelielinux.org/"

	if [ "${CBUILD}" != "${CHOST}" ]; then
		for i in man/*.x; do
			touch "${i%x}1"
		done
	fi
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	# stdbuf(1) requires a /usr/libexec/ .so
	# env(1) needs to be in /usr/bin/ for compatibility with the rest of the world
	# install(1) " "
	mkdir -p "$pkgdir"/usr/bin
	for USR_PROGRAM in env install stdbuf; do
		mv "$pkgdir"/bin/$USR_PROGRAM "$pkgdir"/usr/bin/
	done
}

sha512sums="1c8f3584efd61b4b02e7ac5db8e103b63cfb2063432caaf1e64cb2dcc56d8c657d1133bbf10bd41468d6a1f31142e6caa81d16ae68fa3e6e84075c253613a145  coreutils-8.32.tar.xz
96eecaaea757c0a39136733b6b5a9d97b869c50aa41d2052a293544a59a959de7dfa0412f5613e52ff946da42df5b50403b30c88c0a1a882690219fd4ee2efa9  cp-dont-delete.patch
bd8b5cecba59eba30458c8afa895877d39a987b989bc06b70cd6836e0ef684677aaadcb4949b58a713779fe1df1e15e3903e9be14e306671b86b69018b75de8b  disable-csplit-io-err-test.patch
595be7f580f8906467624959c7eddbd6d7007571d92668b6a1ea5543b73764035b44b02ab5f352c67ec6562a368f220af445edd0a0965fb9826bccfd25ddbdba  disable-mbrtowc-test.patch
fd97fccd661befc558b3afb3e32c82dd2cef511a05e6129d49540599297c1b59ab1f109e63a12f585a2348c26c28fb98330c348829d1fe61cf8149d0dd1c989c  gnulib-tests-dont-require-gpg-passphrase.patch
08f6de0e4c1615503a276fb7eb8abb3f1a996d3e9e4c789deb1507e9ac66364e3993591e37ee444d239c64bf3b7d61f909c0da5ead3047b75623cd846e9f868f  gnulib-test-fixes.patch
b1509e5678a05f24d6e764c047546e5e34a7fbd5edb59c7debedb144a0096d8ac247c7e2722a5f68c90751e5280bec743c9a6ed3e1433c1916294d68d7bca109  localename-test-fix.patch
c7e7a03ed074d74fa836eaa6372d170b50d5719914160d9a835db25fb958e1fcfa4530231abd8f9b2323958a510750a8a5f65a5cea56555fa8a39dd8abd6e8cc  ls-removed-dirs.patch
0b90b7721dd01bd22eba50f73bc756930dd4a8bc78ec408ce7f155a02b520375023897b9d12872030735d908208b912ea7adfac9e8b4d055e617873d982eef56  test-df-symlink-bindmount.patch"
[ "${CBUILD}" != "${CHOST}" ] && sha512sums="
bae804f057252c25452ac178c545dc2c4b4775cbfbdcfd4775edd1a4ed6507882bfac39e2b11ad01b74230ca48d761bf62f11bde5bcbc35a6e5a61cbe4a46e36  051_all_coreutils-mangen.patch
"
return 0
