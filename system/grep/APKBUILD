# Contributor: Michael Mason <ms13sp@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=grep
pkgver=3.4
pkgrel=0
pkgdesc="Pattern matching utilities"
url="https://www.gnu.org/software/grep/grep.html"
arch="all"
license="GPL-3.0+"
depends=""
checkdepends="coreutils bash diffutils"
makedepends="pcre-dev autoconf automake"
subpackages="$pkgname-doc $pkgname-lang"
source="https://mirrors.kernel.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz
	gnulib-tests-dont-require-gpg-passphrase.patch
	fix-tests.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--bindir=/bin \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	rm -rf "$pkgdir"/usr/lib/charset.alias
	rmdir -p "$pkgdir"/usr/lib 2>/dev/null || true
}

sha512sums="0f1506bd19971fbdcb47a111277ca63e8ad045456f096980852fd0a61c860f29f4b369bbaaa5cbce4b0a81718e3e3274d9a078b491f2109baa9a02ce600ee206  grep-3.4.tar.xz
fd97fccd661befc558b3afb3e32c82dd2cef511a05e6129d49540599297c1b59ab1f109e63a12f585a2348c26c28fb98330c348829d1fe61cf8149d0dd1c989c  gnulib-tests-dont-require-gpg-passphrase.patch
9ba6b01c0c74933299afb469dadd2ea0c7e24befa34c691671a576063e32a1f0c735541e5e2bb0073d8afd814790909f7f895827aa8a2fbacdfcae380a7bcb11  fix-tests.patch"
