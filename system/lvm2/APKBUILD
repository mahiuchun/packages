# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=lvm2
pkgver=2.03.09
pkgrel=0
pkgdesc="Logical Volume Manager 2 utilities"
url="https://sourceware.org/lvm2/"
arch="all"
options="!check"  # Test tries to use real /dev/mapper/control.
license="GPL-2.0+ AND LGPL-2.1-only"
# lvm2 will dlopen libs so we need to add explicit depend
depends="lvm2-libs=$pkgver-r$pkgrel util-linux"
depends_dev="linux-headers"
makedepends_build="bash coreutils libaio-dev"
makedepends_host="$depends_dev util-linux-dev"
makedepends="$makedepends_build $makedepends_host"
replaces="device-mapper $pkgname-dmeventd"
source="https://mirrors.kernel.org/sourceware/$pkgname/LVM2.$pkgver.tgz
	fix-stdio-usage.patch
	mallinfo.patch
	mlockall-default-config.patch
	lvm.initd
	lvm.confd
	dmeventd.initd
	"
subpackages="
	$pkgname-dev
	$pkgname-doc
	$pkgname-openrc
	$pkgname-libs
	$pkgname-udev
	"

builddir="$srcdir/LVM2.$pkgver"

build () {
	# during cross-compilation malloc test goes wrong
	export ac_cv_func_malloc_0_nonnull=yes
	export ac_cv_func_realloc_0_nonnull=yes

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--libdir=/lib \
		--sbindir=/sbin \
		--localstatedir=/var \
		--disable-readline \
		--enable-pkgconfig \
		--with-thin=internal \
		--enable-dmeventd \
		--enable-cmdlib \
		--with-default-dm-run-dir=/run \
		--with-default-locking-dir=/run/lock/lvm \
		--with-default-pid-dir=/run \
		--with-default-run-dir=/run/lvm \
		--with-thin-check=/sbin/thin_check \
		--with-thin-dump=/sbin/thin_dump \
		--with-thin-repair=/sbin/thin_repair \
		--with-dmeventd-path=/sbin/dmeventd
	make SHELL=/bin/bash
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	install -d "$pkgdir"/etc/lvm/archive "$pkgdir"/etc/lvm/backup
	install -Dm755 "$srcdir"/lvm.initd "$pkgdir"/etc/init.d/lvm
	install -Dm644 "$srcdir"/lvm.confd "$pkgdir"/etc/conf.d/lvm
	install -Dm755 "$srcdir"/dmeventd.initd "$pkgdir"/etc/init.d/dmeventd
}

libs() {
	replaces="device-mapper-libs device-mapper-event-libs"
	default_libs

	mv "$pkgdir/lib/device-mapper" "$subpkgdir/lib/"
}

udev() {
	pkgdesc="udev rules for LVM and device mapper"
	install_if="$pkgname=$pkgver-r$pkgrel eudev"

	make -C "$builddir"/udev
	install -m644 -Dt "$subpkgdir"/lib/udev/rules.d/ \
		"$builddir"/udev/*.rules
}


sha512sums="8540e46a6025ab14b592ccd9493b3224ffc0f962739a0a8de6d7b25c65c6ad96fc83ddb0e3d877b123a5e1b104476d0c20ccee2ead6d322257ec82ad1e3362d4  LVM2.2.03.09.tgz
bc4473c2f48ff6cab121f92211cd1b67589a6abadd8e32ffa366fed6ddbf9c87be695aa54a6e97b5b79203bf1a7d22cdb5974da84d38b4977592ef7a061612e7  fix-stdio-usage.patch
9272ec8c5184ef5dc776ead8f74132e072b7563b5119a3a38b712f00d92a1e3878c9b3a54eb2b01dcba038110c686b39d4c17ecd0eb258537e9217d7ed03c408  mallinfo.patch
d190c40a137b006d7b63298069c93ff08d2804b990e85d44739cd7c48beec9a569903b98f0d940895fc7365723ba886acd7ef0e08f1f65a1a391d1c448ce080e  mlockall-default-config.patch
a853078660fd2fd943538924f56e81dc5793294e26b8f61d93e6188893f15f4a438d33792b341c1865d61e03f4a371b7c7ee0db5f4130ef7cb7aeaeb9290086a  lvm.initd
07caf8fa942290f3c953cc2463aaf55bac01d0bcb9351daf3880fa4d0eefb67fe00761c46a7a4da91cd2f8e492a12fed35853a15dc939cd80d19d3303bc3951d  lvm.confd
ca06220065525e93347efb7a5746a367cf55cb03574e50eed29bd7c4eae036bdd199b52c42d81811b6e742649e73ab8e66525f663bed66f9d1d82d0efde50332  dmeventd.initd"
