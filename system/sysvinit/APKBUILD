# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sysvinit
pkgver=2.88
pkgrel=8
pkgdesc="System V-style init programs"
url="https://savannah.nongnu.org/projects/sysvinit"
arch="all"
license="GPL-2.0+"
depends="s6-linux-init-common"
makedepends="linux-headers utmps-dev"
install="sysvinit.post-upgrade sysvinit.post-install"
options="!check"
provides="/sbin/init=0"
subpackages="$pkgname-doc"
source="https://download.savannah.nongnu.org/releases/sysvinit/sysvinit-${pkgver}dsf.tar.bz2
	inittab-2.88
	sysvinit-2.88-posix-header.patch
	utmpx.patch
	s6-svscanboot
	"
builddir="$srcdir/sysvinit-${pkgver}dsf"

prepare() {
	default_prepare

	# util-linux
	sed -i -r \
		-e '/^(USR)?S?BIN/s:\<(last|lastb|mesg)\>::g' \
		-e '/^MAN[18]/s:\<(last|lastb|mesg)[.][18]\>::g' \
		src/Makefile

	# broken
	sed -i -r \
		-e '/^USRBIN/s:utmpdump::g' \
		-e '/^MAN1/s:utmpdump\.1::g' \
		src/Makefile

	# procps
	sed -i -r \
		-e '/\/bin\/pidof/d'\
		-e '/^MAN8/s:\<pidof.8\>::g' \
		src/Makefile
}

build() {
	export DISTRO="Adélie"
	make -C src
}

_install_s6_stuff() {
	svcimg="$pkgdir/etc/s6-linux-init/current/run-image/service"
	mkdir -p -m 0755 "$pkgdir/sbin" "$svcimg/.s6-svscan" "$svcimg/s6-svscan-log"
	{ echo '#!/bin/execlineb -P' ; echo 'false' ; } > "$svcimg/.s6-svscan/crash"
	chmod 0755 "$svcimg/.s6-svscan/crash"
	{ echo '#!/bin/execlineb -P' ; echo 's6-svc -x -- /run/service/s6-svscan-log' ; } > "$svcimg/.s6-svscan/finish"
	chmod 0755 "$svcimg/.s6-svscan/finish"
	{ echo '#!/bin/execlineb -P' ; echo 'redirfd -rnb 0 fifo' ; echo 's6-setuidgid catchlog' ; echo 's6-log -bd3 -- t /run/uncaught-logs' ; } > "$svcimg/s6-svscan-log/run"
	chmod 0755 "$svcimg/s6-svscan-log/run"
	install -D -m 0755 "$srcdir"/s6-svscanboot "$pkgdir/sbin/s6-svscanboot"
}

package() {
	make -C src install ROOT="$pkgdir"
	rm "$pkgdir"/usr/bin/lastb || true
	install -D -m644 "$srcdir"/inittab-2.88 "$pkgdir"/etc/inittab
	_install_s6_stuff
}

sha512sums="0bd8eeb124e84fdfa8e621b05f796804ee69a9076b65f5115826bfa814ac1d5d28d31a5c22ebe77c86a93b2288edf4891adc0afaecc4de656c4ecda8a83807bf  sysvinit-2.88dsf.tar.bz2
87668b49690091a227c0384fd2400f1006d24c27cc27a25efa7eba56839ccb1eead00b58ce4b654eab9c0208d68aa2cbb888fd5f2990905845aa9688442a69a0  inittab-2.88
27dfe089660a291cbcba06d8564bad11f7fd7c96629e72c2b005562689dc7d8bb479c760e980590906e98423b991ae0acd048713d3bc372174d55ed894abeb3f  sysvinit-2.88-posix-header.patch
3605f88ac3faf7d12bf2269ca5d8625850d53e8583b573ab280fa17066c8e4e5217a0d17b94e47ea67a153ad3b88b433471a77544bd085f01f7d9d353ac16aae  utmpx.patch
e52fd49daa5abfc583f1973f3428b1e00a71e7136a8bc6418e94b345d53ef250b3b3c3bee389fe37872b26a78d0957ae852e221428f33b2c728dfd3d50b59634  s6-svscanboot"
