# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=s6-rc
pkgver=0.5.2.1
pkgrel=0
pkgdesc="skarnet.org's service manager, working on top of s6"
url="https://skarnet.org/software/s6-rc/"
arch="all"
options="!check"
license="ISC"
_skalibs_version=2.10.0.0
_s6_version=2.10.0.0
depends="execline"
makedepends="skalibs-dev>=$_skalibs_version execline-dev s6-dev>=$_s6_version"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--enable-shared \
		--enable-static \
		--enable-allstatic \
		--enable-static-libc \
		--libdir=/usr/lib \
		--libexecdir="/lib/$pkgname" \
		--with-dynlib=/lib
	make
}

package() {
	make DESTDIR="$pkgdir" install
}


libs() {
        pkgdesc="$pkgdesc (shared libraries)"
        depends="skalibs-libs>=$_skalibs_version s6-libs>=$_s6_version"
        mkdir -p "$subpkgdir/lib"
        mv "$pkgdir"/lib/*.so.* "$subpkgdir/lib/"
}


dev() {
        pkgdesc="$pkgdesc (development files)"
        depends="skalibs-dev>=$_skalibs_version s6-dev>=$_s6_version"
	install_if="dev $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr/include" "$subpkgdir/usr/lib"
        mv "$pkgdir/usr/include" "$subpkgdir/usr/"
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir/usr/lib/"
}


libsdev() {
        pkgdesc="$pkgdesc (development files for dynamic linking)"
        depends="$pkgname-dev"
        mkdir -p "$subpkgdir/lib"
        mv "$pkgdir"/lib/*.so "$subpkgdir/lib/"
}


doc() {
        pkgdesc="$pkgdesc (documentation)"
        depends=""
        install_if="docs $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr/share/doc"
        cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="0cbd48e79b7f0e24528b6dda1df6980ca3dcebca7aecf408ad69490aacced4a425b5bcdb1a68b5bfa46199b7a05364a80158db5722cd6506d8d833da125c1fec  s6-rc-0.5.2.1.tar.gz"
